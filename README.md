# Conteneur Docker pour le serveur Web utilisé à l'IUT

Vous trouverez dans ce dépôt : 
* le fichier de configuration `Dockerfile` d'une image Docker contenant un serveur Web et tous les outils nécessaires pour les différents cours de développement Web à l'IUT. 
* un tutoriel d'installation de Docker et de création d'un conteneur serveur Web
* un dossier `public_html` dans lequel il faudra enregistrer vos pages Web. 

## Tutoriel

### Premier contact

Comme c'est normalement la première fois que vous utilisez Docker, commencez par le
[tutoriel d'installation et de configuration du conteneur Docker qui inclura le serveur Web](TutorielInstallation.md).

Si vous n'arrivez pas à faire marcher Docker dans un délai raisonnable (~ 30
minutes), passez à la section *Alternatives* plus bas. Vous reprendrez
l'installation en dehors des cours.

### Au démarrage de votre machine 

Si vous avez déjà installé Docker et lancé le serveur Web, voici ce qu'il faut faire après chaque redémarrage de machine : 

1. Si vous êtes sur **Ubuntu 24.04**, exécutez la commande suivante afin que
   Docker Desktop puisse se lancer 
   ```bash
   sudo sysctl -w kernel.apparmor_restrict_unprivileged_userns=0
   ```
1. Démarrer Docker Desktop
2. Dans l'onglet Gauche *Containers*, relancez votre conteneur à l'aide du
   bouton *Play* ![BoutonPlay](./img/BoutonPlay.png).
  ![ConteneurArrete](./img/ConteneurArrete.png)

## Alternative à Docker 

L'alternative à Docker est de copier vos pages Web sur votre espace personnel à l'IUT, en
utilisant soit FTP avec FileZilla (recommandé pour les débutants), soit SSH (cf.
[instructions sur l'intranet Côté Technique > Accès au Réseau > Depuis chez
vous](https://iutdepinfo.iutmontp.univ-montp2.fr/intranet/acces-aux-serveurs/)).
Vos pages Web seront alors servies à l'adresse
[http://webinfo.iutmontp.univ-montp2.fr/~mon_login_iut/](http://webinfo.iutmontp.univ-montp2.fr/~mon_login_iut/).

Cette alternative doit plutôt être temporaire, car nous écrivons les sujets de
TDs en considérant que vous utilisez le serveur Web conteneurisé fourni. 

## Logiciels installés sur le conteneur

Pour information, l'image est basée sur [l'image de serveur Web PHP 8.3 avec Apache fournie par
DockerHub](https://hub.docker.com/_/php/).

### Pour les cours de Web du semestre 3

* Connecteurs PHP Database Object (PDO) pour communiquer avec des bases de données MySQL et Oracle
* Gestionnaire des droits ACL
* XDebug : Débugueur PHP 
* Configuration de PHP pour le développement

### Pour les cours de Web du semestre 4

* Connecteurs PHP Database Object (PDO) pour communiquer avec des bases de données PostGreSQL et SQLite
* Composer : Gestionnaire de paquets PHP

### Pour les cours de Web du semestre 5

* NPM : Gestionnaire de paquets JavaScript